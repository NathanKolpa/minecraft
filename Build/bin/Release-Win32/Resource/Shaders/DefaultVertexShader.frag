#version 440 core

in vec2 out_textureCoord;

uniform sampler2D  uni_texture;


void main()
{
	gl_FragColor = texture(uni_texture, out_textureCoord);
}