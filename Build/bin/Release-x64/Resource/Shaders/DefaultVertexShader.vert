#version 440 core

layout(location = 0) in vec3 in_positions;
layout(location = 1) in vec2 in_textureCoords;

out vec2 out_textureCoord;

uniform mat4 uni_projection;
uniform mat4 uni_view;

void main()
{
	mat4 model = uni_projection * uni_view;
	gl_Position = model * vec4(in_positions, 1.0);

	out_textureCoord = in_textureCoords;
}