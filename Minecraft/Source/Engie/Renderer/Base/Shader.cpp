#include "Shader.h"
#include "../../Core/ErrorHandle.h"
#include <fstream>
#include <string>
#include <GL\glew.h>


orb::Shader::Shader()
{}

orb::Shader::Shader(const char* vertexShaderFile, const char* fragmentShaderFile)
{
	loadShader(vertexShaderFile, fragmentShaderFile);
}


orb::Shader::~Shader()
{}

std::string ParseFile(std::string fileName)
{
	std::string filePath = "Resource/Shaders/";
	filePath.append(fileName);
	std::ifstream file(filePath);

	std::string output;

	if (!file.is_open())
	{
		logError("Could not open ShaderFile \"" << filePath << "\"");
	}

	std::string line;
	while (std::getline(file, line))
	{
		output.append(line);
		output.append("\n");
	}

	file.close();
	return output;
}

GLuint CreateShader(std::string& shaderCode, unsigned int type, const char* shaderName)
{
	GLuint shaderID = glCreateShader(type);
	const char* sourceCode = shaderCode.c_str();
	GLfnc(glShaderSource(shaderID, 1, &sourceCode, nullptr));
	GLfnc(glCompileShader(shaderID));

	//error handeling
	int status;
	GLfnc(glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status));
	if (status == GL_FALSE)
	{
		int messageLength;
		GLfnc(glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &messageLength));
		char* message = new char[messageLength];
		GLfnc(glGetShaderInfoLog(shaderID, messageLength, &messageLength, message));

		setConsoleColor(4);
		std::cout << "<===== Shader Failed To Compile =====>" << std::endl;
		std::cout << "At Shader: " << shaderName << std::endl;
		std::cout << "Error Message: " << message;
		std::cout << "<====================================>" << std::endl;

		ErrorBreak();
		delete message;
		setConsoleColor(7);
	}

	return shaderID;
}

void orb::Shader::loadShader(const char* vertexShaderFile, const char* fragmentShaderFile)
{
	std::string vertexShaderString = ParseFile(vertexShaderFile);
	std::string fragmentShaderString = ParseFile(fragmentShaderFile);

	GLuint program = glCreateProgram();
	GLuint vertShader = CreateShader(vertexShaderString, GL_VERTEX_SHADER, vertexShaderFile);
	GLuint fragShader = CreateShader(fragmentShaderString, GL_FRAGMENT_SHADER, fragmentShaderFile);

	GLfnc(glAttachShader(program, vertShader));
	GLfnc(glAttachShader(program, fragShader));

	GLfnc(glLinkProgram(program));
	GLfnc(glValidateProgram(program));

	m_programID = program;
	logMessage("shader loaded");
}



unsigned int orb::Shader::getShaderID()
{
	return m_programID;
}

void orb::Shader::bind()
{
	GLfnc(glUseProgram(m_programID));
}

void orb::Shader::unbind()
{
	GLfnc(glUseProgram(0));
}



//uniforms

void orb::Shader::setUniform1i(const char* name, int Value)
{
	GLfnc(glUniform1i(glGetUniformLocation(m_programID, name), Value));
}

void orb::Shader::setUniform1f(const char * name, float Value)
{
	GLfnc(glUniform1f(glGetUniformLocation(m_programID, name), Value));
}

void orb::Shader::setUniformVec2(const char * name, glm::vec2 Value)
{
	GLfnc(glUniform2f(glGetUniformLocation(m_programID, name), Value.x, Value.y));
}

void orb::Shader::setUniformVec3(const char* name, glm::vec3 Value)
{
	GLfnc(glUniform3f(glGetUniformLocation(m_programID, name), Value.x, Value.y, Value.z));
}

void orb::Shader::setUniformVec4(const char* name, glm::vec4 Value)
{

}

void orb::Shader::setUniformMat4(const char* name, glm::mat4 Value)
{
	GLfnc(glUniformMatrix4fv(glGetUniformLocation(m_programID, name), 1, GL_FALSE, &Value[0][0]));
}
