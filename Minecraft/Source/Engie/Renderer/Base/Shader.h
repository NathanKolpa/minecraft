#pragma once
#include <glm/glm/glm.hpp>

namespace orb
{
	class Shader
	{
	public:
		Shader();
		Shader(const char* vertexShaderFile, const char* fragmentShaderFile);
		~Shader();
	public:
		void loadShader(const char* vertexShaderFile, const char* fragmentShaderFile);

		void setUniform1i(const char* name, int Value);
		void setUniform1f(const char* name, float Value);
		void setUniformVec2(const char* name, glm::vec2 Value);
		void setUniformVec3(const char* name, glm::vec3 Value);
		void setUniformVec4(const char* name, glm::vec4 Value);
		void setUniformMat4(const char* name, glm::mat4 Value);

		unsigned int getShaderID();
		void bind();
		void unbind();
	private:
		unsigned int m_programID;
	};

}