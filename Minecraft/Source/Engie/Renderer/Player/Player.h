#pragma once
#include <GLFW/glfw3.h>
#include <glm/glm/glm.hpp>

namespace orb
{
	static class Player
	{
	private:
		static glm::vec3 m_position;
		static glm::vec3 m_rotation;

		static GLFWwindow* m_window;
		static bool cameraMouseEnabled;

		float m_moveSpeed;
		float m_sensitivity = 0.01;
	public:
		Player(GLFWwindow* window);
		~Player();
	private:
		void forceCameraRestrictions();
		void updateMouse();
		void keyboard();
	public:
		void setCameraMouseEnabled(bool flag);
		glm::mat4 getCameraMatrix();
		void updateMovement();
	};
}