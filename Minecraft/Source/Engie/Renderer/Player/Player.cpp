#include "Player.h"
#include <iostream>
#include <glm/glm/gtc/matrix_transform.hpp>
#include "../Engie/Core/Time.h"
#include "../../Core/Constants.h"

glm::vec3 orb::Player::m_position;
glm::vec3 orb::Player::m_rotation;
GLFWwindow* orb::Player::m_window;
bool orb::Player::cameraMouseEnabled;

orb::Player::Player(GLFWwindow* window)
{
	m_moveSpeed = 10;
	m_window = window;

	m_position.y = SURFACE_HEIGHT;

	setCameraMouseEnabled(true);
}

orb::Player::~Player()
{
}

void orb::Player::setCameraMouseEnabled(bool flag)
{
	if (flag)
	{
		//reset the cursor
		int width, height;
		glfwGetWindowSize(m_window, &width, &height);
		glfwSetCursorPos(m_window, width / 2, height / 2);

		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		cameraMouseEnabled = true;
	}
	else
	{
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		cameraMouseEnabled = false;
	}
}

glm::mat4 orb::Player::getCameraMatrix()
{

	glm::vec3 cameraFront = glm::vec3(0.0, 0.0, -1.0f);
	glm::mat4 view = glm::lookAt(m_position, m_position + cameraFront, glm::vec3(0, 1, 0));

	view = glm::rotate(glm::mat4(1.0f), m_rotation.z, glm::vec3(1, 0, 0));
	view = glm::rotate(view, m_rotation.y, glm::vec3(0, 1, 0));
	view = glm::rotate(view, m_rotation.x, glm::vec3(0, 0, 1));
	view = glm::translate(view, glm::vec3(-m_position.x, -m_position.y, -m_position.z));

	return view;
}

void orb::Player::updateMovement()
{
	updateMouse();

	keyboard();
}

void orb::Player::forceCameraRestrictions()
{
	if (m_rotation.z < glm::radians(-91.0f))
		m_rotation.z = glm::radians(-91.0f);

	if (m_rotation.z > glm::radians(91.0f))
		m_rotation.z = glm::radians(91.0f);

	if (m_rotation.y > glm::radians(360.0f))
		m_rotation.y -= glm::radians(360.0f);

	if (m_rotation.y < glm::radians(-360.0f))
		m_rotation.y += glm::radians(360.0f);
}

void orb::Player::updateMouse()
{
	if (cameraMouseEnabled)
	{
		//get mid sizes of the window
		int midWindowWidth;
		int midWindowHeight;
		{
			int width, height;
			glfwGetWindowSize(m_window, &width, &height);

			midWindowWidth = width / 2;
			midWindowHeight = height / 2;
		}

		//update the rotations
		double mouseX, mouseY;
		glfwGetCursorPos(m_window, &mouseX, &mouseY);

		double horizMovement = (mouseX - midWindowWidth) * m_sensitivity;
		double vertMovement = (mouseY - midWindowHeight) * m_sensitivity;


		m_rotation.z += vertMovement;
		m_rotation.y += horizMovement;

		//set the cursor position
		glfwSetCursorPos(m_window, midWindowWidth, midWindowHeight);

		forceCameraRestrictions();
	}
}


bool lastESCState = false;
void orb::Player::keyboard()
{
	//enable / disable mouse
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE))//TODO: move this to a player event handle
	{
		if (!lastESCState)
		{
			if (cameraMouseEnabled)
				setCameraMouseEnabled(false);
			else
				setCameraMouseEnabled(true);
		}
		lastESCState = true;
	}
	else
	{
		lastESCState = false;
	}

	//movement
	if (glfwGetKey(m_window, GLFW_KEY_W))
	{
		m_position.z -= cos(m_rotation.y) * m_moveSpeed * Time::deltaTime();
		m_position.x += sin(m_rotation.y) * m_moveSpeed * Time::deltaTime();
	}

	if (glfwGetKey(m_window, GLFW_KEY_S))
	{
		m_position.z += cos(m_rotation.y) * m_moveSpeed * Time::deltaTime();
		m_position.x -= sin(m_rotation.y) * m_moveSpeed * Time::deltaTime();
	}

	if (glfwGetKey(m_window, GLFW_KEY_D))
	{
		m_position.z -= cos(m_rotation.y + 1.5f) * m_moveSpeed * Time::deltaTime();
		m_position.x += sin(m_rotation.y + 1.5f) * m_moveSpeed * Time::deltaTime();
	}

	if (glfwGetKey(m_window, GLFW_KEY_A))
	{
		m_position.z += cos(m_rotation.y + 1.5f) * m_moveSpeed * Time::deltaTime();
		m_position.x -= sin(m_rotation.y + 1.5f) * m_moveSpeed * Time::deltaTime();
	}

	if (glfwGetKey(m_window, GLFW_KEY_SPACE))
	{
		m_position.y += m_moveSpeed * Time::deltaTime();
	}

	if (glfwGetKey(m_window, GLFW_KEY_LEFT_SHIFT))
	{
		m_position.y -= m_moveSpeed * Time::deltaTime();
	}
}
