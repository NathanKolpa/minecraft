#pragma once
#include <vector>
#include "../../Game/Blocks.h"
#include "../Base/Shader.h"
#include "../Loading/Mesh.h"
#include "../Loading/Texture.h"

namespace orb
{
	class Chunk
	{
	private:
		std::vector<const RenderObject*> m_chuckData;
		Mesh m_model;

		int positionX = 0, positionZ = 0;
		bool isLoaded = false;

		Texture* m_texture = nullptr;

	private:
		int getIndex(int x, int y, int z);
	public:
		Chunk(Texture& texture, int x, int y);
		~Chunk();
	private:
#pragma region add*
		void addTextureCoords(std::vector<float>& texturecoords, glm::vec2 offset);
		void addTop(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z);
		void addBottom(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z);
		void addRight(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z);
		void addLeft(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z);
		void addFront(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z);
		void addBack(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z);
#pragma endregion
	public:
		void removeBlock();
		void addBlock();//dit moet de mesh efficient updaten
		void setBlock(int x, int y, int z, RenderObject* block);
		const RenderObject* getBlock(int x, int y, int z);

		void draw();
		void updateMesh(Chunk* front);//TODO: thread
	};
}