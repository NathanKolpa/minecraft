#include "Chunk.h"
#include "../../Core/Constants.h"
#include "../../Core/ErrorHandle.h"
#include <GL/glew.h>



orb::Chunk::Chunk(Texture& texture, int x, int z)
{

	m_texture = &texture;
	positionX = x;
	positionZ = z;

	for (int y = 0; y < CHUCK_HEIGHT; y++)
	{
		for (int x = 0; x < CHUNK_WIDTH; x++)
		{
			for (int z = 0; z < CHUNK_WIDTH; z++)
			{

				m_chuckData.push_back(blocks::pointers::air);
			}
		}
	}

}

orb::Chunk::~Chunk()
{
}



int orb::Chunk::getIndex(int x, int y, int z)								//na de atlas toevoegen alleen dingen opschonen
{
	return x + CHUNK_WIDTH * (y + CHUCK_HEIGHT * z);
}


void orb::Chunk::setBlock(int x, int y, int z, RenderObject* block)
{
	m_chuckData[getIndex(x, y, z)] = block;
}

const orb::RenderObject* orb::Chunk::getBlock(int x, int y, int z)
{
	return m_chuckData[getIndex(x, y, z)];
}

void orb::Chunk::draw()
{
	if (isLoaded)
	{

		m_model.bind();
		GLfnc(glEnableVertexAttribArray(0));
		GLfnc(glEnableVertexAttribArray(1));
		//GLfnc(glDrawElements(GL_QUADS, m_model.getVertexCount(), GL_UNSIGNED_INT, (void*)0));
		GLfnc(glDrawArrays(GL_QUADS, 0, m_model.getVertexCount()));
		GLfnc(glBindVertexArray(0));
		m_model.unBind();
	}
}

void orb::Chunk::updateMesh(Chunk* front)											//dit moet beter kunnen denk ik
{


	std::vector<float> positions;
	std::vector<float> texcoords;

	for (int y = 0; y < CHUCK_HEIGHT; y++)
		for (int x = 0; x < CHUNK_WIDTH; x++)
			for (int z = 0; z < CHUNK_WIDTH; z++)
			{
				int i = getIndex(x, y, z);
				if (m_chuckData[i]->isDraweble())
				{
					//top
					if (y + 1 < CHUCK_HEIGHT)
					{
						if (!m_chuckData[getIndex(x, y + 1, z)]->isCube())
							addTop(positions, texcoords, x, y, z);
					}
					else
					{
						addTop(positions, texcoords, x, y, z);
					}

					//bottom
					if (y - 1 >= 0)
					{
						if (!m_chuckData[getIndex(x, y - 1, z)]->isCube())
							addBottom(positions, texcoords, x, y, z);
					}
					else
					{
						addBottom(positions, texcoords, x, y, z);
					}

					//right
					if (x + 1 < CHUNK_WIDTH)
					{
						if (!m_chuckData[getIndex(x + 1, y, z)]->isCube())
							addRight(positions, texcoords, x, y, z);
					}
					else
					{
						addRight(positions, texcoords, x, y, z);
					}

					//left
					if (x - 1 >= 0)
					{
						if (!m_chuckData[getIndex(x - 1, y, z)]->isCube())
							addLeft(positions, texcoords, x, y, z);
					}
					else
					{
						addLeft(positions, texcoords, x, y, z);
					}


					//front
					if (z + 1 < CHUNK_WIDTH)
					{
						if (!m_chuckData[getIndex(x, y, z + 1)]->isCube())
							addFront(positions, texcoords, x, y, z);
					}
					else
					{
						if (front == nullptr)
						{
							addFront(positions, texcoords, x, y, z);
						}
						else
						{
							if (!front->getBlock(x,y,CHUNK_WIDTH -1)->isDraweble())
							{
								//addFront(positions, texcoords, x, y, z);
							}
						}
					}


					//back
					if (z - 1 >= 0)
					{
						if (!m_chuckData[getIndex(x, y, z - 1)]->isCube())
							addBack(positions, texcoords, x, y, z);
					}
					else
					{
						addBack(positions, texcoords, x, y, z);
					}
				}
			}

	if (isLoaded)
	{
		m_model.changeData(positions, texcoords);//fix de texture
	}
	else
	{
		m_model.loadData(positions, texcoords);
	}
	isLoaded = true;
}




























void orb::Chunk::addTextureCoords(std::vector<float>& texturecoords, glm::vec2 offset)
{
	float width = 1.0f / (float)m_texture->getRowCount();
	glm::vec2 topTextureOffset(offset.x / (float)m_texture->getRowCount(), offset.y / (float)m_texture->getRowCount());

	texturecoords.push_back(topTextureOffset.x);
	texturecoords.push_back(topTextureOffset.y);

	texturecoords.push_back(topTextureOffset.x);
	texturecoords.push_back(topTextureOffset.y + width);

	texturecoords.push_back(topTextureOffset.x + width);
	texturecoords.push_back(topTextureOffset.y + width);

	texturecoords.push_back(topTextureOffset.x + width);
	texturecoords.push_back(topTextureOffset.y);
}

void orb::Chunk::addTop(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z)
{
	textureIndicies offets = m_chuckData[getIndex(x, y, z)]->getTextureIndex();
	addTextureCoords(texturecoords, offets.top);

	x += positionX * CHUNK_WIDTH;
	z += positionZ * CHUNK_WIDTH;

	positions.push_back(x);
	positions.push_back(y);
	positions.push_back(z);

	positions.push_back(x);
	positions.push_back(y);
	positions.push_back(z + 1);

	positions.push_back(x + 1);
	positions.push_back(y);
	positions.push_back(z + 1);

	positions.push_back(x + 1);
	positions.push_back(y);
	positions.push_back(z);

}

void orb::Chunk::addBottom(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z)
{
	textureIndicies offets = m_chuckData[getIndex(x, y, z)]->getTextureIndex();
	addTextureCoords(texturecoords, offets.bottom);

	x += positionX * CHUNK_WIDTH;
	z += positionZ * CHUNK_WIDTH;

	positions.push_back(x + 1);
	positions.push_back(y - 1);
	positions.push_back(z);

	positions.push_back(x + 1);
	positions.push_back(y - 1);
	positions.push_back(z + 1);

	positions.push_back(x);
	positions.push_back(y - 1);
	positions.push_back(z + 1);

	positions.push_back(x);
	positions.push_back(y - 1);
	positions.push_back(z);

}

void orb::Chunk::addRight(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z)
{
	textureIndicies offets = m_chuckData[getIndex(x, y, z)]->getTextureIndex();
	addTextureCoords(texturecoords, offets.side);

	x += positionX * CHUNK_WIDTH;
	z += positionZ * CHUNK_WIDTH;

	positions.push_back(x + 1);
	positions.push_back(y);
	positions.push_back(z + 1);

	positions.push_back(x + 1);
	positions.push_back(y - 1);
	positions.push_back(z + 1);

	positions.push_back(x + 1);
	positions.push_back(y - 1);
	positions.push_back(z);

	positions.push_back(x + 1);
	positions.push_back(y);
	positions.push_back(z);
}

void orb::Chunk::addLeft(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z)
{
	textureIndicies offets = m_chuckData[getIndex(x, y, z)]->getTextureIndex();
	addTextureCoords(texturecoords, offets.side);

	x += positionX * CHUNK_WIDTH;
	z += positionZ * CHUNK_WIDTH;

	positions.push_back(x);
	positions.push_back(y);
	positions.push_back(z);

	positions.push_back(x);
	positions.push_back(y - 1);
	positions.push_back(z);

	positions.push_back(x);
	positions.push_back(y - 1);
	positions.push_back(z + 1);

	positions.push_back(x);
	positions.push_back(y);
	positions.push_back(z + 1);


}

void orb::Chunk::addFront(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z)
{
	textureIndicies offets = m_chuckData[getIndex(x, y, z)]->getTextureIndex();
	addTextureCoords(texturecoords, offets.front);

	x += positionX * CHUNK_WIDTH;
	z += positionZ * CHUNK_WIDTH;

	positions.push_back(x);
	positions.push_back(y);
	positions.push_back(z + 1);

	positions.push_back(x);
	positions.push_back(y - 1);
	positions.push_back(z + 1);

	positions.push_back(x + 1);
	positions.push_back(y - 1);
	positions.push_back(z + 1);

	positions.push_back(x + 1);
	positions.push_back(y);
	positions.push_back(z + 1);

}

void orb::Chunk::addBack(std::vector<float>& positions, std::vector<float>& texturecoords, int x, int y, int z)
{
	textureIndicies offets = m_chuckData[getIndex(x, y, z)]->getTextureIndex();
	addTextureCoords(texturecoords, offets.side);

	x += positionX * CHUNK_WIDTH;
	z += positionZ * CHUNK_WIDTH;

	positions.push_back(x + 1);
	positions.push_back(y);
	positions.push_back(z);

	positions.push_back(x + 1);
	positions.push_back(y - 1);
	positions.push_back(z);

	positions.push_back(x);
	positions.push_back(y - 1);
	positions.push_back(z);

	positions.push_back(x);
	positions.push_back(y);
	positions.push_back(z);

}