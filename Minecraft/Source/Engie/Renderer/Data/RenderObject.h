#pragma once
#include <vector>
#include <string>
#include <glm/glm/glm.hpp>

namespace orb
{
	struct textureIndicies
	{
		glm::vec2 top;
		glm::vec2 side;
		glm::vec2 front;
		glm::vec2 bottom;
	};

	class RenderObject
	{
	public:
		RenderObject()
		{
		}

		virtual bool isDraweble() const
		{
			return false;
		}

		virtual bool isCube()const
		{
			return false;
		}

		virtual textureIndicies getTextureIndex() const
		{
			return textureIndicies();
		}

		virtual unsigned int getTexture()const
		{
			return 0;
		}

		virtual unsigned int getVAO()const
		{
			return 0;
		}
	};

	class Block : public RenderObject
	{
	private:
		unsigned int m_VAO = 0;
		unsigned int m_Texture = 0;
	public:
		Block()
		{

		}

		bool isDraweble() const override
		{
			return true;
		}

		bool isCube() const override
		{
			return true;
		}

		textureIndicies getTextureIndex() const override
		{
			textureIndicies temp;
			temp.top = glm::vec2(0, 0);
			temp.side = glm::vec2(1, 0);
			temp.side = glm::vec2(0, 1);
			return temp;
		}

		unsigned int getVAO() const override
		{
			return 0;
		}
	};
}

