#pragma once
#include <vector>

namespace orb
{
	class Mesh
	{
	private:
		unsigned int m_VAO;
		//unsigned int m_EBO;
		std::vector<unsigned int>m_VBOBuffers;

		int m_vertexCount = 0;
	public:
		Mesh();
		~Mesh();
	private:
		void storeDataInAttribList(std::vector<float> data, int stride, int vertexCount);
		void deleteBuffers();
	public:
		void bind();
		void unBind();
		int getVertexCount();

		void loadData(std::vector<float> positions, std::vector<float> textureCoords);
		void changeData(std::vector<float> positions, std::vector<float> textureCoords);
	};
}