#include "Mesh.h"
#include "../../Core/ErrorHandle.h"


orb::Mesh::Mesh()
{
}

orb::Mesh::~Mesh()
{
}

void orb::Mesh::bind()
{
	//GLfnc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO));
	GLfnc(glBindVertexArray(m_VAO));
}

void orb::Mesh::unBind()
{
	//GLfnc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
	GLfnc(glBindVertexArray(0));
}

int orb::Mesh::getVertexCount()
{
	return m_vertexCount;
}

void orb::Mesh::deleteBuffers()
{
}

void orb::Mesh::storeDataInAttribList(std::vector<float> data, int stride, int vertexCount)
{
	//generate VBO
	unsigned int vboID;
	GLfnc(glGenBuffers(1, &vboID));

	//bind vbo
	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, vboID));
	GLfnc(glEnableVertexAttribArray(stride));

	//store data
	GLfnc(glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), data.data(), GL_DYNAMIC_DRAW));
	GLfnc(glVertexAttribPointer(stride, vertexCount, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0));

	//undbind vbo
	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, 0));
	m_VBOBuffers.push_back(vboID);
}



void orb::Mesh::loadData(std::vector<float> positions, std::vector<float> textureCoords)
{
	//generate VAO
	GLfnc(glGenVertexArrays(1, &m_VAO));
	GLfnc(glBindVertexArray(m_VAO));

	//store model data in vao
	storeDataInAttribList(positions, 0, 3);
	storeDataInAttribList(textureCoords, 1, 2);


	//unbind and return
	GLfnc(glBindVertexArray(0));

	m_vertexCount = positions.size() / 3;
}

void orb::Mesh::changeData(std::vector<float> positions, std::vector<float> textureCoords)
{
	GLfnc(glBindVertexArray(m_VAO));

	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, m_VBOBuffers[0]));
	GLfnc(glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(float), positions.data(), GL_DYNAMIC_DRAW));
	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, 0));

	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, m_VBOBuffers[1]));
	GLfnc(glBufferData(GL_ARRAY_BUFFER, textureCoords.size() * sizeof(float), textureCoords.data(), GL_DYNAMIC_DRAW));
	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, 0));

	GLfnc(glBindVertexArray(0));

	m_vertexCount = positions.size() / 3;
}
