#pragma once

namespace orb
{
	class Texture
	{
	private:
		unsigned int textureID;
		int width, height, bitsPerPixel;
		bool Loaded = false;

		int numberOfRows = 1;
	public:
		Texture();
		Texture(const char* imageName, int Rows);
		Texture(unsigned int texture, int width, int height, int BPS, int rows);
		~Texture();

		void bind(int textureSlot);
		void unbind();

		int getRowCount();
	};
}