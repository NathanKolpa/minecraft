#include "Texture.h"
#include "../../Core/ErrorHandle.h"
#include <GL\glew.h>
#include <string>
#include <stb_image.h>

#define ORB_PNG 1
#define ORB_JPG 1

orb::Texture::Texture()
{
	Loaded = false;
}

unsigned int loadOpenGLTexture(const char* image, int &out_Width, int &out_Height, int &out_BPP, bool overridePath);

orb::Texture::Texture(const char* imageName, int Rows)
{
	textureID = loadOpenGLTexture(imageName, width, height, bitsPerPixel, false);
	numberOfRows = Rows;
	Loaded = true;
}

orb::Texture::Texture(unsigned int texture, int width, int height, int BPS, int Rows)
{
	width = width;
	height = height;
	bitsPerPixel = BPS;

	textureID = texture;
	numberOfRows = Rows;
	Loaded = true;
}


orb::Texture::~Texture()
{}

void orb::Texture::bind(int textureSlot)
{
	GLfnc(glActiveTexture(GL_TEXTURE0 + textureSlot));
	GLfnc(glBindTexture(GL_TEXTURE_2D, textureID));
}

void orb::Texture::unbind()
{
	GLfnc(glBindTexture(GL_TEXTURE_2D, 0));
}

int orb::Texture::getRowCount()
{
	return numberOfRows;
}

unsigned int loadOpenGLTexture(const char* image, int &out_Width, int &out_Height, int &out_BPP, bool overridePath)
{
	std::string filePath = image;

	if (!overridePath)
	{
		filePath = "Resource/Textures/";
		filePath.append(image);
	}

	stbi_set_flip_vertically_on_load(false);
	unsigned char* localbuffer = stbi_load(filePath.c_str(), &out_Width, &out_Height, &out_BPP, 0);

	if (!localbuffer)
	{
		logWarning("Could not open Image \"" << image << "\"");
	}

	//check if the image is png or jpg
	int imageType;

	{
		std::string imageName = image;
		std::string imageTypeS;
		bool typeStarted = false;
		for (int i = 0; i < imageName.length(); i++)
		{
			if (typeStarted)
				imageTypeS += imageName[i];

			if (imageName[i] == '.')
				typeStarted = true;
		}

		if (imageTypeS == "jpg")
			imageType = ORB_JPG;
		else if (imageTypeS == "png")
			imageType = ORB_PNG;
		else
		{
			logWarning("Invalid image format \"" << imageTypeS.c_str() << '\"');
		}
	}

	//set the correct settings
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	GLfnc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, imageType == ORB_JPG ? GL_REPEAT : GL_CLAMP_TO_EDGE));
	GLfnc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, imageType == ORB_JPG ? GL_REPEAT : GL_CLAMP_TO_EDGE));
	GLfnc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	GLfnc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

	if (localbuffer)
	{
		//load to the coresponding format
		if (imageType == ORB_PNG)
		{
			GLfnc(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, out_Width, out_Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, localbuffer));
		}
		else if (imageType == ORB_JPG)
		{
			GLfnc(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, out_Width, out_Height, 0, GL_RGB, GL_UNSIGNED_BYTE, localbuffer));
		}

		GLfnc(glGenerateMipmap(GL_TEXTURE_2D));
	}

	//clean up
	GLfnc(glBindTexture(GL_TEXTURE_2D, 0));
	if (localbuffer != 0)
		stbi_image_free(localbuffer);
	return textureID;
}