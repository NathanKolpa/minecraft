#include "ChunkGenerator.h"
#include "../../Core/Constants.h"
#include <PerlinNoise.h>
#include <cmath>
#include <ctime>

orb::ChunkGenerator::ChunkGenerator()
{
}

orb::ChunkGenerator::ChunkGenerator(Texture& texture)
{
	m_texture = &texture;
}

orb::ChunkGenerator::~ChunkGenerator()
{
}

void orb::ChunkGenerator::fillRow(Chunk& chunk, int x, int z, int height)
{
	for (int i = 0; i < height; i++)
	{
		if (i >= height - 1)
		{
			chunk.setBlock(x, i, z, blocks::pointers::grass);
		}
		else if (i >= height - 1 - 3)
		{
			chunk.setBlock(x, i, z, blocks::pointers::dirt);
		}
		else if (i == 0)
		{
			chunk.setBlock(x, i, z, blocks::pointers::bedrock);
		}
		else
		{
			chunk.setBlock(x, i, z, blocks::pointers::stone);
		}
	}
}

double clamp(double x, double upper, double lower)
{
	return fmin(upper, fmax(x, lower));
}

orb::Chunk orb::ChunkGenerator::generateChunk(int chunkX, int chunkZ)
{
	Chunk chunk(*m_texture, chunkX, chunkZ);

	siv::PerlinNoise noise(123);

	double frequency = 8;
	frequency = clamp(frequency, 1, 64.0);

	int octaves = 1;
	octaves = clamp(octaves, 1, 64);

	const double fx = 16 / frequency;
	const double fz = 16 / frequency;


	for (int x = 0; x < CHUNK_WIDTH; x++)
	{
		for (int z = 0; z < CHUNK_WIDTH; z++)
		{
			double height = noise.octaveNoise0_1((x + CHUNK_WIDTH * chunkX) / fx, (z + CHUNK_WIDTH * chunkZ) / fz, octaves) * 15;
			height += SURFACE_HEIGHT;
			fillRow(chunk, x, z, height);
		}
	}

	return chunk;
}
