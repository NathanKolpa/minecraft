#pragma once
#include "../../Renderer/Data/Chunk.h"
#include "../../Renderer/Loading/Texture.h"
namespace orb
{
	class ChunkGenerator
	{
	private:
		Texture* m_texture = nullptr;
	public:
		ChunkGenerator();
		ChunkGenerator(Texture& texture);
		~ChunkGenerator();
	private:
		void fillRow(Chunk& chunk, int x, int z, int height);
	public:
		Chunk generateChunk(int chunkX, int chunkZ);//in chunk coords
	};
}