#include "Game.h"
#include "../Core/Constants.h"
#include <glm/glm/gtc/matrix_transform.hpp>
#include <glm/glm/gtx/transform.hpp>

orb::Game::Game()
	:m_window(1280, 720, "minecraft but better"),
	m_player(m_window.getWindowPointer())
{


	m_window.setBackgroundColor(glm::vec4(0, 0.83, 1, 1));
}

orb::Game::~Game()
{
}

void orb::Game::updateProjection()
{
	m_projection = glm::perspective((float)glm::radians(CAMERA_FOV), (float)m_window.getWidth() / (float)m_window.getHeight(), 0.1f, 10000.0f);
}

void orb::Game::run()
{

	updateProjection();

	while (m_window.isActive())
	{
		m_window.prepare();

		//game logic
		m_player.updateMovement();

		//rendering


		m_world.draw(m_projection, m_player.getCameraMatrix());


		m_window.update();
	}
}
