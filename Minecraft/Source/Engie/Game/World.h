#pragma once
#include "../Renderer/Data/Chunk.h"
#include "../Renderer/Base/Shader.h"
#include "Generation/ChunkGenerator.h"

namespace orb
{
	class World
	{
	private://world moet een pointer zijn
		Shader m_shader;
		Texture m_textureAtlas1;
		ChunkGenerator m_worldGen;
		std::vector<Chunk> m_activeChunks;
		int m_chunkRange = 10;					// 70 = 15fps
	public:
		World();
		~World();
	public:
		void setBlock(int x, int y, int z);//in world positions
		void updateEverything();
		void removeBlock();
		void draw(glm::mat4 projection, glm::mat4 camera);
	};
}