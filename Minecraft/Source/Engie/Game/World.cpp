#include "World.h"

orb::World::World()
{
	m_shader.loadShader("DefaultVertexShader.vert", "DefaultVertexShader.frag");
	m_textureAtlas1 = Texture("Default.png", 16);
	m_worldGen = ChunkGenerator(m_textureAtlas1);

	for (int x = 0; x < m_chunkRange; x++)
	{
		for (int z = 0; z < m_chunkRange; z++)
		{
			m_activeChunks.push_back(m_worldGen.generateChunk(x, z));
		}
	}
	for (int x = 0; x < m_chunkRange; x++)
	{
		for (int z = 0; z < m_chunkRange; z++)
		{

			Chunk* front = nullptr;
			if (z + 1 < m_chunkRange)
				front = &m_activeChunks[x + m_chunkRange * (z + 1)];

			m_activeChunks[x + m_chunkRange * z].updateMesh(front);
		}
	}
}

orb::World::~World()
{
}

void orb::World::updateEverything()
{
}

void orb::World::draw(glm::mat4 projection, glm::mat4 camera)
{
	m_shader.bind();

	//set uniforms
	m_shader.setUniformMat4("uni_projection", projection);
	m_shader.setUniformMat4("uni_view", camera);
	m_shader.setUniform1i("uni_texture", 1);

	m_textureAtlas1.bind(1);

	//draw
	for (int i = 0; i < m_activeChunks.size(); i++)
	{
		m_activeChunks[i].draw();
	}

	//unbind
	m_textureAtlas1.unbind();
	m_shader.unbind();
}