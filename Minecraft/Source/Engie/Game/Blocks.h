#pragma once
#include "../Renderer/Data/RenderObject.h"
namespace orb
{
	namespace blocks
	{
		class Air : public Block
		{
			bool isDraweble() const override
			{
				return false;
			}

			bool isCube() const override
			{
				return false;
			}

			textureIndicies getTextureIndex() const override
			{
				return textureIndicies();
			}
		};

		class Debug : public Block
		{
			textureIndicies getTextureIndex() const override
			{
				textureIndicies temp;
				temp.top = glm::vec2(0, 0);
				temp.side = glm::vec2(1, 0);
				temp.front = glm::vec2(1, 1);
				temp.bottom = glm::vec2(0, 1);
				return temp;
			}
		};

		class Grass : public Block
		{
			textureIndicies getTextureIndex() const override
			{
				textureIndicies temp;
				temp.top = glm::vec2(0, 0);
				temp.side = glm::vec2(3, 0);
				temp.front = glm::vec2(3, 0);
				temp.bottom = glm::vec2(2, 0);
				return temp;
			}
		};

		class Dirt : public Block
		{
			textureIndicies getTextureIndex() const override
			{
				textureIndicies temp;
				temp.top = glm::vec2(2, 0);
				temp.side = glm::vec2(2, 0);
				temp.front = glm::vec2(2, 0);
				temp.bottom = glm::vec2(2, 0);
				return temp;
			}
		};

		class Stone : public Block
		{
			textureIndicies getTextureIndex() const override
			{
				textureIndicies temp;
				temp.top = glm::vec2(1, 0);
				temp.side = glm::vec2(1, 0);
				temp.front = glm::vec2(1, 0);
				temp.bottom = glm::vec2(1, 0);
				return temp;
			}
		};

		class Bedrock : public Block
		{
			textureIndicies getTextureIndex() const override
			{
				textureIndicies temp;
				temp.top = glm::vec2(1, 1);
				temp.side = glm::vec2(1, 1);
				temp.front = glm::vec2(1, 1);
				temp.bottom = glm::vec2(1, 1);
				return temp;
			}
		};
	}
}

namespace orb
{
	namespace blocks
	{
		namespace pointers
		{
			static Air* air = new Air;
			static Debug* debug = new Debug;
			static Grass* grass = new Grass;
			static Dirt* dirt = new Dirt;
			static Stone* stone = new Stone;
			static Bedrock* bedrock = new Bedrock;
		}
	}
}