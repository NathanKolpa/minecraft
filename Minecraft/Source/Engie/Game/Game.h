#pragma once
#include "../Core/Window.h"
#include "World.h"
#include "../Engie/Renderer/Player/Player.h"
#include <glm/glm/glm.hpp>

namespace orb
{
	class Game
	{
	private:
		glm::mat4 m_projection;
		Window m_window;

		World m_world;
		Player m_player;
	public:
		Game();
		~Game();
	private:
		void updateProjection();
	public:
		void run();
	};
}