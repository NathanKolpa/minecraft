#include "Time.h"


double* orb::Time::m_deltaTimePointer;
void orb::Time::setDeltaTimePointer(double* deltaTime)
{
	m_deltaTimePointer = deltaTime;
}

double orb::Time::deltaTime()
{
	return *m_deltaTimePointer;
}

int orb::Time::framesPerSecond()
{
	return (int)(1.0 / *m_deltaTimePointer);
}
