#include "Window.h"
#include "../Core/ErrorHandle.h"
#include "Time.h"

void orb::Window::setBackgroundColor(glm::vec4 color)
{
	m_backgroundColor = color;
}

int orb::Window::getWidth()
{
	return m_windowWidth;
}

int orb::Window::getHeight()
{
	return m_windowHeight;
}

GLFWwindow * orb::Window::getWindowPointer()
{
	return m_Window;
}

/*
* initialize opengl and create a window
*/
orb::Window::Window(int width, int height, std::string name)
{
	//check if glfw is valid
	if (!glfwInit())
	{
		logError("Failed to initialize GLFW");
		std::cin.get();
		return;
	}


	m_Window = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
	glfwMakeContextCurrent(m_Window);
	
	//init opengl
	if (glewInit() != GLEW_OK)
	{
		logError("Failed to initialize GLEW");
		std::cin.get();
		return;
	}
	logMessage("OpenGL Version [" << glGetString(GL_VERSION) << ']');

	//the rest of the contructor
	m_isActive = true;
	m_windowWidth = width;
	m_windowHeight = height;
	m_windowName = name;
	glfwSetWindowSizeLimits(m_Window, 50, 50, GLFW_DONT_CARE, GLFW_DONT_CARE);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	Time::setDeltaTimePointer(&m_deltaTime);
}

orb::Window::~Window()
{
	glfwTerminate();
}

void orb::Window::handleTime()
{
	m_previousTime = m_currentTime;
	m_currentTime = glfwGetTime();
	m_deltaTime = m_currentTime - m_previousTime;

	_framecount++;
	_elapsedTime += m_deltaTime;
	_totalfps += Time::framesPerSecond();

	if (_elapsedTime > 0.25)
	{
		std::string newTitle = m_windowName + " - FPS: " + std::to_string(_totalfps / _framecount);
		glfwSetWindowTitle(m_Window, newTitle.c_str());

		_elapsedTime = 0;
		_totalfps = 0;
		_framecount = 0;
	}
}


/*
* dictates if the gameloop should be running
*/
bool orb::Window::isActive()
{
	return !glfwWindowShouldClose(m_Window) && m_isActive;
}


void orb::Window::prepare()
{
	handleTime();
	glfwPollEvents();

	//reize the window
	{
		int newWidth;
		int newHeight;

		glfwGetFramebufferSize(m_Window, &newWidth, &newHeight);
		if (newWidth != 0 && newHeight != 0 && (newWidth != m_windowWidth || newHeight != m_windowHeight))
		{
			glViewport(0, 0, newWidth, newHeight);
			m_windowWidth = newWidth;
			m_windowHeight = newHeight;
			logMessage("resized to (" << newWidth << ", " << newHeight << ")");
		}

	}

	GLfnc(glClearColor(m_backgroundColor.x, m_backgroundColor.y, m_backgroundColor.z, m_backgroundColor.w));
	GLfnc(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void orb::Window::update()
{
	glfwSwapBuffers(m_Window);
}
