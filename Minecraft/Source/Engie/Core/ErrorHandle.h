#pragma once
#include <iostream>
#include <Windows.h>
#include <GL/glew.h>

#if defined(WIN32)

static void setConsoleColor(int color)
{
	HANDLE ConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(ConsoleHandle, color);
}

#define logError(x) setConsoleColor(4); std::cout << "ERROR: " << x << std::endl; setConsoleColor(7)
#define logWarning(x) setConsoleColor(5); std::cout << "WARNING: " << x << std::endl; setConsoleColor(7)
#define logMessage(x) setConsoleColor(7); std::cout << "ENGIE: " << x << std::endl; setConsoleColor(7)


#else
#error this function has only a windows inplementation. feel free to implement it yourself for your platform
#endif


#ifdef _DEBUG
/*
* Call this function with every opengl function
* This function Will check errors with every call
* This function Will output a error message when a error is detected
*/
#define ASSERT(x) if(!(x)) ErrorBreak();
#define GLfnc(x) ClearGLError(); x; ASSERT(GLLogCall(#x, __FILE__, __LINE__))

#else
/*
* For preformance sake it will not check errors when in release
*/
#define GLfnc(x) x;

#endif

static void ClearGLError()
{
	while (glGetError() != GL_NO_ERROR);
}

static void ErrorBreak()
{
	logMessage("Would you like to exit the program? (y/n)");

	char ans;
	std::cin >> ans;

	if (ans == 'y' || ans == 'Y')
	{
		exit(-1);
	}
}

static bool GLLogCall(const char* Function, const char* File, int line)
{

	while (GLenum error = glGetError())
	{
		HANDLE ConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

		SetConsoleTextAttribute(ConsoleHandle, 4);

		std::cout << std::endl;
		std::cout << "<===== OpenGL ERROR =====>" << std::endl;
		std::cout << "Error Code: " << error << std::endl;
		std::cout << "In file: " << File << std::endl;
		std::cout << "At: line " << line << std::endl;
		std::cout << "Called By: " << Function << ';' << std::endl;
		std::cout << "<===============================>" << std::endl;

		SetConsoleTextAttribute(ConsoleHandle, 7);
		return false;
	}

	return true;

}