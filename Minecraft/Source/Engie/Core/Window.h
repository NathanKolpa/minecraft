#pragma once
#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm/glm.hpp>

namespace orb
{
	class Window
	{
	private:
		int m_windowWidth, m_windowHeight;
		std::string m_windowName;
		GLFWwindow* m_Window;
		bool m_isActive = false;
		glm::vec4 m_backgroundColor;

		double m_previousTime = glfwGetTime();
		double m_currentTime;
		double m_deltaTime = 0;
		unsigned int _framecount = 0;
		unsigned int _totalfps = 0;
		double _elapsedTime = 0;
	public:
		void setBackgroundColor(glm::vec4 color);

		int getWidth();
		int getHeight();
		GLFWwindow* getWindowPointer();

		double getFPS();	//use global variables instead
		double getDeltaTime();
	public:
		Window(int width, int height, std::string name);
		~Window();
	private:
		void handleTime();
	public:
		bool isActive();
		void prepare();
		void update();
	};
}
